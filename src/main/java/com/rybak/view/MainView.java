package com.rybak.view;

import com.rybak.controller.AirlineController;
import com.rybak.controller.AirlineControllerImpl;
import com.rybak.model.Plane;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MainView {


    private static Scanner INPUT = new Scanner(System.in);

    private AirlineController airlineController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;


    public MainView() {
        airlineController = new AirlineControllerImpl();

        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - show all planes");
        menu.put("2", " 2 - add new plane");
        menu.put("3", " 3 - show planes by type");
        menu.put("4", " 4 - sort by distance");
        menu.put("5", " 5 - calculate total load capacity");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }


    private void pressButton1() {
        System.out.println("All planes of airline:");
        List<Plane> list = airlineController.showAllPlanes();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }
    }

    private void pressButton2() {
        System.out.println("Please select type of plane: ");
        System.out.println("1 - passenger");
        System.out.println("2 - cargo");
        int i = INPUT.nextInt();
        String type = "";
        if (i == 1) {
            type = "passenger";
            System.out.println("Please enter fuel: ");
            int f = INPUT.nextInt();
            System.out.println("Please enter distance: ");
            int d = INPUT.nextInt();
            System.out.println("Please enter passenger: ");
            int p = INPUT.nextInt();
            airlineController.addNewPassengerPlane(type, f, d, p);
        } else if (i == 2) {
            type = "cargo";
            System.out.println("Please enter fuel: ");
            int f = INPUT.nextInt();
            System.out.println("Please enter distance: ");
            int d = INPUT.nextInt();
            System.out.println("Please enter load capacity: ");
            int l = INPUT.nextInt();
            airlineController.addNewCargoPlane(type, f, d, l);
        } else System.out.println("wrong type");

    }

    private void pressButton3() {
        System.out.println("Please select type of plane: ");
        System.out.println("1 - passenger");
        System.out.println("2 - cargo");
        int i = INPUT.nextInt();
        StringBuilder type = new StringBuilder();
        if (i == 1) {
            type.append("passenger");
        } else if (i == 2) {
            type.append("cargo");
        } else {
            System.out.println("Wrong type");
            pressButton3();
        }


        List<Plane> list = airlineController.selectPlaneByType(type.toString());

        list.forEach(System.out::println);
    }

    private void pressButton4() {
        System.out.println("Planes sorted by max distance:");
        List<Plane> list = airlineController.sortByDistance();
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        }
    }

    private void pressButton5() {
        int total = airlineController.getTotalLoadCapacity();
        System.out.println("Total load capacity of Cargo planes= " + total);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println(" Q - quit");
            System.out.print("Please, select menu point: ");
            keyMenu = INPUT.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        INPUT.close();
        System.exit(0);
    }


}
