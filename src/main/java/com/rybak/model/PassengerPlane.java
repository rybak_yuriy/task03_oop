package com.rybak.model;

public class PassengerPlane extends Plane {

    private int maxPassengerCapacity;


    public PassengerPlane(String type, int fuelConsumption, int maxFlightDistance, int maxPassengerCapacity) {
        super(type, fuelConsumption, maxFlightDistance);
        this.maxPassengerCapacity = maxPassengerCapacity;
    }

    @Override
    public String toString() {
        return "PassengerPlane{" +
                "type= " + type +
                ", maxFlightDistance= " + maxFlightDistance +
                ", fuelConsumption= " + fuelConsumption +
                ", maxPassengerCapacity= " + maxPassengerCapacity +
                '}';
    }

    public int getMaxPassengerCapacity() {
        return maxPassengerCapacity;
    }

    public void setMaxPassengerCapacity(int maxPassengerCapacity) {
        this.maxPassengerCapacity = maxPassengerCapacity;
    }
}
