package com.rybak.model;

public class Pilot extends StuffMember {

    public Pilot(String name, int experience) {
        super(name, experience);
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "name='" + name + '\'' +
                ", experience=" + experience +
                '}';
    }
}
