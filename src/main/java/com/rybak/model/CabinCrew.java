package com.rybak.model;

public class CabinCrew extends StuffMember {

    public CabinCrew(String name, int experience) {
        super(name, experience);
    }

    @Override
    public String toString() {
        return "CabinCrew{" +
                "name='" + name + '\'' +
                ", experience=" + experience +
                '}';
    }
}
