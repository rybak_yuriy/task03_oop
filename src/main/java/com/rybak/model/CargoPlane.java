package com.rybak.model;

public class CargoPlane extends Plane {

    private int maxPayload;

    public CargoPlane(String type, int fuelConsumption, int maxFlightDistance, int maxPayload) {
        super(type, fuelConsumption, maxFlightDistance);
        this.maxPayload = maxPayload;
    }

    @Override
    public String toString() {
        return "CargoPlane{" +
                "type= " + type +
                ", maxFlightDistance= " + maxFlightDistance +
                ", fuelConsumption= " + fuelConsumption +
                ", maxPayload= " + maxPayload +
                '}';
    }

    public int getLoadCapacity() {
        return maxPayload;
    }

    public void setLoadCapacity(int loadCapacity) {
        this.maxPayload = loadCapacity;
    }
}
