package com.rybak.model;

import java.util.ArrayList;
import java.util.List;

public class Airline {

    private String name;
    private CargoPlane cargoPlane;

    private List<Plane> planes = new ArrayList<>();
    private List<StuffMember> stuffMembers = new ArrayList<>();

    public Airline(String name) {
        this.name = name;
    }

    public Airline() {
    }


    public void savePlane(Plane plane){
        planes.add(plane);
    }

    public void saveStuff(StuffMember stuffMember){
        stuffMembers.add(stuffMember);
    }


    public List<Plane> getPlanes() {
        return planes;
    }

    public int getLoadCapacity(CargoPlane plane) {
        return plane.getLoadCapacity();
    }
}
