package com.rybak.model.businesslogic;

import com.rybak.model.Airline;
import com.rybak.model.CargoPlane;
import com.rybak.model.PassengerPlane;
import com.rybak.model.Plane;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BusinessLogic implements Model {
    private Airline airline;

    public BusinessLogic() {
        airline = new Airline("rym-airline");
    }

    @Override
    public void addNewPassengerPlane(String type, int fuel, int distance, int pass) {
        Plane passengerPlane = new PassengerPlane(type, fuel, distance, pass);
        airline.savePlane(passengerPlane);
    }

    @Override
    public void addNewCargoPlane(String type, int fuel, int distance, int load) {
        Plane cargoPlane = new CargoPlane(type, fuel, distance, load);
        airline.savePlane(cargoPlane);
    }


    public List<Plane> showAllPlanes() {

        return airline.getPlanes();
    }

    public List<Plane> selectPlanesByType(String type) {
        return airline.getPlanes().stream()
                .filter(plane -> plane.getType().equals(type))
                .collect(Collectors.toList());
    }

    @Override
    public List<Plane> sortPlanesByMaxDistance() {
        List<Plane> list = airline.getPlanes();
        Collections.sort(list, Comparator.comparingInt(Plane::getMaxFlightDistance));
        return list;
    }

    @Override
    public int getTotalLoadCapacity() {
        int total = 0;
        List<Plane> list = airline.getPlanes();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getType().equals("cargo")) {
                total += airline.getLoadCapacity((CargoPlane) list.get(i));

            }
        }
        return total;
    }


}
