package com.rybak.model.businesslogic;

import com.rybak.model.Plane;

import java.util.List;

public interface Model {

    void addNewPassengerPlane(String type, int fuel, int distance, int pass);

    void addNewCargoPlane(String type, int fuel, int distance, int load);

    List<Plane> showAllPlanes();

    List<Plane> selectPlanesByType(String type);

    List<Plane> sortPlanesByMaxDistance();

    int getTotalLoadCapacity ();

}

