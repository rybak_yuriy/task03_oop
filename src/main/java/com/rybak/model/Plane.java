package com.rybak.model;

public abstract class Plane {

    int fuelConsumption;
    int maxFlightDistance;
    String type;

    public Plane(String type, int fuelConsumption, int maxFlightDistance) {
        this.type = type;
        this.fuelConsumption = fuelConsumption;
        this.maxFlightDistance = maxFlightDistance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getMaxFlightDistance() {
        return maxFlightDistance;
    }

    public void setMaxFlightDistance(int maxFlightDistance) {
        this.maxFlightDistance = maxFlightDistance;
    }
}
