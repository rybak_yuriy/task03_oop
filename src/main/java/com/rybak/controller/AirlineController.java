package com.rybak.controller;

import com.rybak.model.Plane;

import java.util.List;

public interface AirlineController {


    void addNewPassengerPlane(String type, int fuel, int distance, int load);

    void addNewCargoPlane(String type, int fuel, int distance, int load);

    List<Plane> showAllPlanes();

    List<Plane> selectPlaneByType(String type);

    List<Plane> sortByDistance();

    int getTotalLoadCapacity();
}
