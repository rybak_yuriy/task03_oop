package com.rybak.controller;

import com.rybak.model.Plane;
import com.rybak.model.businesslogic.BusinessLogic;
import com.rybak.model.businesslogic.Model;

import java.util.List;

public class AirlineControllerImpl implements AirlineController {

    private Model model;

    public AirlineControllerImpl() {
        model = new BusinessLogic();
    }


    @Override
    public void addNewPassengerPlane(String type, int fuel, int distance, int load) {
        model.addNewPassengerPlane(type, fuel, distance, load);
    }

    @Override
    public void addNewCargoPlane(String type, int fuel, int distance, int load) {
        model.addNewCargoPlane(type, fuel, distance, load);
    }


    public List<Plane> showAllPlanes() {
        return model.showAllPlanes();
    }


    public List<Plane> selectPlaneByType(String type) {
        return model.selectPlanesByType(type);
    }

    @Override
    public List<Plane> sortByDistance() {
        return model.sortPlanesByMaxDistance();
    }

    @Override
    public int getTotalLoadCapacity() {
        return model.getTotalLoadCapacity();
    }


}
