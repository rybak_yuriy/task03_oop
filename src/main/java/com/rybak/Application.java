package com.rybak;

import com.rybak.view.MainView;

class Application {

    public static void main(String[] args) {
        MainView mainView = new MainView();
        mainView.start();
    }
}
